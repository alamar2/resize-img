import os
from os import listdir
from os.path import isfile, join, isdir
from PIL import Image


ACCORD_IMGS_ = "C:/Users/Renan/Projetos/crowler-accord/imgs/"
dirs = [d for d in listdir(ACCORD_IMGS_) if isdir(join(ACCORD_IMGS_, d))]
files = []
for d in dirs:
    pasta = join(ACCORD_IMGS_, d)
    for f in listdir(pasta):
        if isfile(join(pasta, f)):
            files.append(d + '/' + f)

size = 350, 350

print(files)


def resize(infile):
    nome_arquivo = os.path.splitext(infile)[0]
    print(f'Realizando resize do arquivo: {nome_arquivo}')
    nome_arquivo = nome_arquivo.replace('/', '_')
    outfile = ACCORD_IMGS_ + 'resize/' + nome_arquivo + '.resize.png'
    try:
        im = Image.open(ACCORD_IMGS_ + infile)
        im.thumbnail(size, Image.ANTIALIAS)
        im.save(outfile, "PNG")
    except IOError:
        print("cannot create thumbnail for '%s'" % infile)


for file in files:
    img_file_size = os.stat(ACCORD_IMGS_ + file).st_size
    print(f'Tamanho: {img_file_size} - Imagem: {file}')
    if img_file_size > 350000:
        resize(file)
